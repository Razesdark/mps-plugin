from typing import List

from mps_youtube.base_types import Event, TransformationEvent

class EventHandlerContextWrapper:
  def __init__(self, context, eh):
    self.__eh = eh
    self.__context = context

  def trigger_event(self, eventName, contextObject):
    name = eventName if eventName.startswith(self.__context + ".") else ".".join([self.__context, eventName])
    self.__eh.trigger_event(name, contextObject)

  def trigger_transformation(self, eventName, contextObject):
    name = eventName if eventName.startswith(self.__context + ".") else ".".join([self.__context, eventName])
    return self.__eh.trigger_transformation(name, contextObject)
  
class EventHandler:
  def __init__(self):
    self.events: List[Event] = list()


  def add_event(self, event):
    self.events.append(event)

  def trigger_event(self, eventName: str, contextObject: object) -> None:
    for event in self.events:
      if event.triggers(context=eventName):
        event.run(contextObject)

  def trigger_transformation(self, event_name: str, context_object: object) -> object:
    for event in self.events:
      if(issubclass(event, TransformationEvent) and event.triggers(context=event_name)):
        context_object = event.run(context_object)

    return context_object
        