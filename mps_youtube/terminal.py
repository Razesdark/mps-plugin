"""
  The object that handles information regarding the terminal
"""
from os import popen

class TerminalInformation:
  """
    An item that holds all relevant information regarding
    the terminal.

  """
  width: int = 0
  height: int = 0

  def __init__(self, width: int, height: int) -> None:
    self.width = width

    # height is set to be one less, to make room for the info line
    self.height = height - 1

  def get_width(self) -> int:
    """Get Width"""
    return self.width

  def get_height(self) -> int:
    """Get Height"""
    return self.width

def get() -> TerminalInformation:
  """
    Gets the current terminal information
  """
  rows, columns = popen('stty size', 'r').read().split()
  return TerminalInformation(int(columns), int(rows))
