""" The view module hold the ViewHandler, ViewHandlerContextWrapper
    The ViewHandler lets you control which views are getting printed on the screen

    An instance of the wrapper is passed to the plugin context to restrict access to some
    functions of the ViewHandler.
"""
from typing import List, Optional, Dict
from mps_youtube.base_types import View
from mps_youtube.colors import c

class ViewHandlerContextWrapper:
  """ViewHandlerContextWrapper stops accidental access to the ViewHandler.
  """
  __context = None
  __viewHander = None

  def colored_text(self, color: str, text: str) -> str:
    return c(color, text)

  def __init__(self, _context, _view):
    self.__context = _context
    self.__vh = _view

  def set_info(self, value):
    """set_info lets you set the information line text
    """
    self.__vh.info_line = value

  def get_info(self):
    """get_info gets you the information line text
    """
    return self.__vh.info_line

  def add_view(self, view):
    """Add a view to the view Stack
    """
    self.__vh.add_view(view)

  def delete_view(self, view=None):
    self.__vh.delete_view(view)

  def stack_depth(self):
    return len(self.__vh.view_stack)

  def current_view(self) -> View:
    """Get the currently top view on the Stack
    """
    return self.__vh.current_view()

  def get_view(self, name):
    """Get a class by name
    """
    return self.__vh.get_view(name)

class ViewHandler:
  view_stack: List[View]
  view_list: Dict

  def __init__(self, core) -> None:
    self.core = core
    self.view_stack = list()
    self.view_list = dict()
    self.info_line = ""

  def add_view(self, view: View) -> None:
    """
      Puts a view on the ViewStack
    """
    self.view_stack.append(view)

  def delete_view(self, view: Optional[View] = None) -> None:
    """
      Deletes a view from the ViewStack
    """
    if view is None:
      self.view_stack.pop()
    else:
      self.view_stack.remove(view)

  def post_plugins(self) -> None:

    event_info = self.core.event_handler.trigger_transformation("core.welcome-screen.create", [
      "Welcome to MPS-youtube plugin edition",
      "Version 0.0.1"
    ])
    self.add_view(
      self.get_view("WelcomeScreen").with_logo(event_info))

  def current_view(self) -> Optional[View]:
    return None if not self.view_stack else self.view_stack[len(self.view_stack) -1]

  #############################################################################
  ### ViewClasses
  ###
  ### These views are not on the display stack, but list of view classes an user
  ### can invoke
  ############################################################################

  def register_view(self, cls: View):
    name = str(cls.__name__).split(".")[-1]
    if name in self.view_list.keys():
      raise KeyError("A view with that name already exists")
    self.view_list[name] = cls

  def get_view(self, cls) -> View:
    if not cls in self.view_list.keys():
      raise KeyError("The view '{}' is not registered with the view handler".format(cls))
    return self.view_list[cls]
