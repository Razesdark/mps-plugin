import re

from mps_youtube.base_types import GlobalCommand
class CommandHandler:
  def __init__(self):
    self.commands = list()
    self.global_commands = list()


  def register_command(self, cls):
    if issubclass(cls, GlobalCommand):
      # is global
      self.global_commands.append(
        cls(cls.REGEX)
      )
    else:
      self.commands.append(
        cls(cls.REGEX)
      )

  def parse_text(self, scope, string) -> bool:
    for command in self.commands:
      if re.match(command.REGEX, string) and command.triggers(scope):
        command.run(*re.match(command.REGEX, string).groups())
        return True
    for command in self.global_commands:
      if command.triggers(string, scope):
        command.run(*re.match(command.REGEX, string).groups())
        return True
    return False
