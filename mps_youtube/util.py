"""
  Assorted collection of functions
"""
import sys
import os
import unicodedata

from typing import Tuple

def terminal_size() -> Tuple[int, int]:
  """
    TODO: MAKE OBSOLETE
  """
  rows, columns = os.popen('stty size', 'r').read().split()
  return int(columns), int(rows)

def uea_pad(num, t, direction="<", notrunc=False):
  """ Right pad with spaces taking into account East Asian width chars. """
  direction = direction.strip() or "<"

  t = ' '.join(t.split('\n'))

  # TODO: Find better way of dealing with this?
  if num <= 0:
    return ''

  if not notrunc:
    # Truncate to max of num characters
    t = t[:num]

  if real_len(t) < num:
    spaces = num - real_len(t)

    if direction == "<":
      t = t + (" " * spaces)

    elif direction == ">":
      t = (" " * spaces) + t

    elif direction == "^":
      right = False

      while real_len(t) < num:
        t = t + " " if right else " " + t
        right = not right

  return t

def real_len(u, alt=False):
  """ Try to determine width of strings displayed with monospace font. """
  if not isinstance(u, str):
    u = u.decode("utf8")

  u = xenc(u) # Handle replacements of unsuported characters

  ueaw = unicodedata.east_asian_width

  if alt:
    # widths = dict(W=2, F=2, A=1, N=0.75, H=0.5)  # original
    widths = dict(N=.75, Na=1, W=2, F=2, A=1)

  else:
    widths = dict(W=2, F=2, A=1, N=1, H=0.5)

  return int(round(sum(widths.get(ueaw(char), 1) for char in u)))

def xenc(stuff):
  """ Replace unsupported characters. """
  sse = sys.stdout.encoding
  if sse != 'UTF-8':
    return str(stuff).encode(sse, "replace").decode(sse)
  return stuff
