"""
  The core of mpsyt

  The core is the main workflow and plugin behaviour handler.
  It handles, views, settings, plugin management and the like.
"""
import sys

from typing import List, Dict, Union

from mps_youtube import terminal
from mps_youtube.base_types import View, SingleDrawView

from mps_youtube.view_handler import ViewHandler
from mps_youtube.plugin_handler import PluginHandler
from mps_youtube.command_handler import CommandHandler
from mps_youtube.event_handler import EventHandler
from mps_youtube.colors import c

class Core:
  """ The central point of the application.
      It holds and controls all state in the application
  """
  view_handler: ViewHandler
  options: Dict[str, str] = dict()

  def __init__(self, options: Dict[str, str]) -> None:
    self.options = options

    self.view_handler = ViewHandler(self)
    self.command_handler = CommandHandler()
    self.event_handler = EventHandler()
    # Plugin handler has to be the last handler to be instantiated
    self.plugin_handler = PluginHandler(self)

    ## TODO Add some events here
    self.view_handler.post_plugins()

    # Perform post-load Event
    self.event_handler.trigger_event('core.post.plugins', None)

    self.shutdown = False


    skip_first_draw = False
    argv = self.parse_arguments()
    if argv:
      for command in argv:
        self.command_handler.parse_text(self.view_handler.current_view().__class__, command)
        self.draw()
        self.draw_info_line()
        skip_first_draw = True
    else:
      self.draw()
      self.draw_info_line()


    while not self.shutdown:
      text = input('[{}]> '.format(c('g', str(len(self.view_handler.view_stack)))))
      found_command = self.command_handler.parse_text(self.view_handler.current_view().__class__, text)
      if not found_command:
        self.view_handler.info_line = "No command bro"
      self.draw()
      self.draw_info_line()

  def draw(self,) -> None:
    """
      Draws the currently shown view on the screen
    """
    current_view = self.view_handler.current_view()

    # add some whitespace
    print("\r\n\r\n")

    if(issubclass(current_view.__class__, View)):
      self.__draw_to_screen(current_view.draw(terminal.get())) 
      if issubclass(current_view.__class__, SingleDrawView):
        self.view_handler.delete_view()
    else:
      # TODO: Replace this error class with a better one
      raise BufferError("Something that is not a view is on the view_stack")

  def __draw_to_screen(self, text: Union[List[str], str]) -> None:
    """ 
      Draws the content of text to screen.

      It currently supports either text, list or view Input
    """
    if(type(text) is text):
      for line in text:
        print(line)
    elif(type(text) is str):
      print(text)
  
  def draw_info_line(self):
    print(self.view_handler.info_line)
  
  def parse_arguments(self):
    return [x.strip() for x in " ".join(sys.argv[1:]).split(";")]