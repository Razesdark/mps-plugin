"""All the base types that are passed along to the plugins contexts
"""

import re

from typing import List

from mps_youtube.terminal import TerminalInformation

class View:
  """Abstract class for views
  """
  def draw(self, terminfo: TerminalInformation) -> str:
    """Draw is run by the core in order to display this view on the
      screen.

      This method should not try to alter screen by itself, but
      return the entire string required to print itself on the screen
    """
    raise NotImplementedError("This is an abstract class")
  
  def __str__(self):
    return self.__class__.__name__

class SingleDrawView(View):
  """
    A View that is drawn only once then discarded.
    Often used when you just want to print text on the screen.
  """
  def draw(self, terminfo: TerminalInformation) -> str:
    raise NotImplementedError("Abstract class")


class Command:
  """
    A View specific command
  """
  def __init__(self, regex):
    self.regex = regex

  def triggers(self, string, scope):
    raise NotImplementedError("triggers must be implemented on all non global commands")
  #def run(self, args, core):
  #  """
  #    The function that is run, if function is triggered
  #  """
  #  raise NotImplementedError("run must be implemented for all commands")

class GlobalCommand(Command):
  """
  A global command
  """
  def triggers(self, string, scope=None) -> bool:
    """Check if this command triggers under this scope,
        with this command
    """
    return re.match(self.regex, string)

class Event:
  """Event is an abstract class that is used when creating objects that responds to events
     triggered by the event handler.

     When a plugin is loaded, if an event is found it will be added to the event pool. When an
     event is triggered in the event handler, it will run trigger() on all registered events
     and run() when trigger() returns true

     An event code looks like 'context.eventName'. All default triggers are in the 'core'
     namespace.

  """

  @staticmethod
  def triggers(context) -> bool:
    """ Checks if this event is triggered in this context

        Parameters
        -----------
        context : str
          The context string in 'context.name' format
    """
    pass

  @staticmethod
  def run(context_object: object) -> None:
    """ The function that is run if event is triggered

        Parameters
        -------------
        contextObject : object
          A dict or object of some sorts that holds all the information supplied with the event
    """
    pass

class TransformationEvent(Event):
  @staticmethod
  def run(context_object:object) -> object:
    raise NotImplementedError

class QueryItem:
  """ Base class for the data returned by the QueryHandler
  """
  data = None

  def __init__(self, data):
    self.data = data

  def __getattr__(self, key):
    return self.data[key] if key in self.data.keys() else None

  def get(self, key):
    """Find a nested key in the dataset by a dot separated path

       For example given the following object structure, 'test' is accessible
       via .get('a.name')
        {
          'a': {
            'name': 'test'
          },
          'b': 23
        }
    """
    splitted = key.split(".")
    tmp = self.data.get(splitted[0], {})
    for key in splitted[1:]:
      tmp = tmp.get(key, {})

    if tmp == {}:
      return None
    return tmp

  def __player__(self):
    return self.get("id.videoId")

  def length(self, _=0):
    """ Returns length of ListViewItem
        A LVI has to return something for length
        even if the item does not have one.
    """
    return 0

  class Player:
    def __init__(self, options):
      self.options = options

    def play(songlist: List[str], shuffle: bool = False, repeat: bool = False) -> None:
      raise NotImplementedError("This method has not been implemented")

    def player_information(self):
      raise NotImplementedError("This method has not been implemented")

    def player_version(self):
      raise NotImplementedError("This method has not been implemented")
