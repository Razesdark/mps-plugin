class TextView(baseTypes.View):
  def __init__(self, text):
    self.page = 0
    self.terminfo = None    
    if(type(text) is str):
      self.text = text.split("\n")
    else:
      self.text = text

    self.text = [x.strip("\r\n") for x in self.text]
  

  def current_page_str(self):
    return str(self.page + 1)

  def max_pages(self):
    return math.ceil(max(1, math.ceil(len(self.text) / self._max_lines_per_page())))

  def max_pages_str(self):
    return str(self.max_pages())

  def draw(self, terminfo):
    self.terminfo = terminfo
    slc = self._page_slice()
    slc = slc + ([' '] * max(0, self._max_lines_per_page() - len(slc)))
    slc.append("Page {} of {}".format(view_handler.colored_text('ul', self.current_page_str()), self.max_pages_str()))
    view_handler.set_info("Type '{}' to close this window".format(view_handler.colored_text('g', 'close')))
    return "\r\n".join(slc)

  def _max_lines_per_page(self):
    return self.terminfo.height - 4
  
  def _page_slice(self):
    gg = self._max_lines_per_page()
    return self.text[self.page*gg:(self.page+1)*gg]