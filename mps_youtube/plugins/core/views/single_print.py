class SinglePrintView(baseTypes.SingleDrawView):
  def __init__(self, data):
    self.data = data
  
  def draw(self, terminfo):
    if isinstance(self.data, list):
      return "\r\n".join(self.data)
    else:
      return str(self.data)
