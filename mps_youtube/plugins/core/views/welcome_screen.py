class WelcomeScreen(baseTypes.View):
  """
    This is the welcoming screen view. 
    It is shown at the start of the program

  """
  welcomeText: typing.List[str]

  def __init__(self, welcomeText: typing.List[str]) -> None:
    self.welcomeText = welcomeText
  
  def draw(self, term_size: baseTypes.TerminalInformation) -> str:
    x,y = term_size.width, term_size.height
    termText = ""

    # Add top spacing
    top_spacing = math.ceil((y - len(self.welcomeText)) / 2)
    termText += "\r\n" * top_spacing

    for line in self.welcomeText:
      # Add left spacing for line
      termText += " " * math.ceil((x / 2) - (len(line) / 2))
      termText += line + "\r\n"
    
    termText += "\r\n" * (y - top_spacing - len(self.welcomeText) - 1)
    return termText
  
  @staticmethod
  def with_logo(freeText: typing.Union[typing.List[str], str]):
    text: typing.List[str] = list()
    text.append(r" __       __  _______    ______   __      __  ________ ")
    text.append(r"/  \     /  |/       \  /      \ /  \    /  |/        |")
    text.append(r"$$  \   /$$ |$$$$$$$  |/$$$$$$  |$$  \  /$$/ $$$$$$$$/ ")
    text.append(r"$$$  \ /$$$ |$$ |__$$ |$$ \__$$/  $$  \/$$/     $$ |   ")
    text.append(r"$$$$  /$$$$ |$$    $$/ $$      \   $$  $$/      $$ |   ")
    text.append(r"$$ $$ $$/$$ |$$$$$$$/   $$$$$$  |   $$$$/       $$ |   ")
    text.append(r"$$ |$$$/ $$ |$$ |      /  \__$$ |    $$ |       $$ |   ")
    text.append(r"$$ | $/  $$ |$$ |      $$    $$/     $$ |       $$ |   ")
    text.append(r"$$/      $$/ $$/        $$$$$$/      $$/        $$/    ")    
    text.append(" ")
    text.append(" ")

    # Add multiple lines, if needed
    if(type(freeText) is list):
      for line in freeText:
        text.append(line)
    else:
      text.append(freeText)

    return view_handler.get_view("WelcomeScreen")(text)                                                   
          