"""

"""
# pylint: disable=C0111,C0112
class Test(baseTypes.View):
  data = ""
  def __init__(self, data):
    self.data = data

  # pylint: disable=W0613
  def draw(self, terminfo: baseTypes.TerminalInformation) -> str:
    return self.data
