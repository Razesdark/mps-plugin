class CoreShowLoadedPluginsInWelcomeScreen(baseTypes.TransformationEvent):
  @staticmethod
  def triggers(context):
    return context == "core.welcome-screen.create"
  
  @staticmethod
  def run(contextObject):
    contextObject.append(" ")
    contextObject.append("{} plugin{} loaded".format(plugin_handler.loaded_plugins(), "" if plugin_handler.loaded_plugins() == 1 else "s"))
    return contextObject