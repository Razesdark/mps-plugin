
class VideoSearchInfoLineEvent(baseTypes.Event):
  @staticmethod
  def triggers(context):
    return context == 'core.post.plugins'
  
  @staticmethod
  def run(contextObject):
    view_handler.set_info("Enter {} to search or [{}]elp".format(
      view_handler.colored_text('r', '/search-term'),
      view_handler.colored_text('r', 'h')))

class VideoSearchCommand(baseTypes.GlobalCommand):
  REGEX = r'^(?:/|search)([\w\s]+)$'

  def run(self, query):
    # Inline delcaration of the search result type has to inherit
    # from baseTypes.QueryItem which has built in logic to populate
    # search queries
    class VideoSearchQueryResult(baseTypes.QueryItem):
      def title(self, mx):
        return uea_pad(mx, self.get("snippet.title"))

      def description(self, mx):
        return uea_pad(mx, self.get('snippet.description'))

      def __str__(self):
        return str(self.get('snippet.title'))
    
    results = Query(VideoSearchQueryResult, 'search', {
      'part': 'id,snippet',
      'q': query,
      'type': 'video',
      'maxResults': 50
    })
    columns = [
      {"name": "idx", "size": 3, "heading": "Num"},
      {"name": "title", "size": 70, "heading": "Title"},
      {"name": "description", "size": "remaining", "heading": "Description"},
    ]
    
    view_handler.add_view(view_handler.get_view("VideoListView")(columns, results, query))