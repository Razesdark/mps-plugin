"""
  Module description
"""
# pylint: disable=E0602
class QuitCommand(baseTypes.GlobalCommand):
  REGEX = r'^(q|quit)'

  def run(self, *matches):
    exit(0)