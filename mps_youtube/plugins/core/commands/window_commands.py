class CloseWindowCommand(baseTypes.GlobalCommand):
  REGEX = r'^c(?:lose)?$'

  def run(self):
    if view_handler.stack_depth() > 1:
      view_handler.delete_view()
    else:
      view_handler.set_info(view_handler.colored_text('r', "Can't close the last window"))


class ListVindowsBuildEvent(baseTypes.TransformationEvent):
  @staticmethod
  def triggers(context):
    return context == 'core.list.build'

  @staticmethod
  def run(context_object):
    context_object.append({
      'cmd': view_handler.colored_text('ul', "list windows"),
      'description': "Lists all views that are opened"
    })
    return context_object

class ListWindows(baseTypes.GlobalCommand):
  REGEX = r'^list windows$'

  def run(self):
    string = ""
    views = [str(x) for x in reversed(view_handler.__dict__['_ViewHandlerContextWrapper__vh'].view_stack)]
    max_length = functools.reduce(lambda x, y: x if x >= y else y, map(len, views)) + 10

    def gen_str(toople):
      _y, _x = toople
      return view_handler.colored_text('g', str(len(views)-_y)) + "\t" + str(_x) + "\r\n"

    views = list(map(gen_str, enumerate(views)))
    string = "Views in stack\r\n" + ("=" * max(18, max_length)) + "\r\n"
    string += "".join(views)

    view = view_handler.get_view('SinglePrintView')
    view_handler.add_view(view(string))
    