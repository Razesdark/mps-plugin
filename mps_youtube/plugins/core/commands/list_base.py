class ListBaseCommand(baseTypes.GlobalCommand):
  REGEX = r'^list$'

  def run(self):

    def string_pad(s, length):
      if len(s) > length:
        return s[0:length]
      elif len(s) < length:
        return s + " " * (length - len(s))
      return s
    # all entries must be dicts that contain { cmd: str, description: str}
    # where cmd is the function with arguments and description is a short describing text
    # core. is infered when triggering this transformation
    events = event_handler.trigger_transformation('list.build', list())

    max_length = functools.reduce(lambda x,y: x if x > y else y, map(lambda x: len(x['cmd']), events))
    max_length += max_length % 4
    text = ""
    for event in events:
      text += string_pad(event['cmd'], max_length) + "\t" + event['description'] + "\r\n"    
    base_text = """Information
=============

There are currently {} commands registered to this namespace

{}
    """.format(len(events), text)

    view = view_handler.get_view('TextView')(base_text.split("\n"))
    view_handler.add_view(view)