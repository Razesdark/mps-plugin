#pylint: disable=E0602
class TestCommand(baseTypes.Command):
  """
    This test function will demonstrate a scope specific function
    
    The command h will only be available in WelcomeScreen and will 
    advance the counter.

    When the counter reaches two, it will spawn a new view(scope) 
    where the h function will not work
  """
  REGEX = r'^test$'

  # TODO: let the scope be a wider array of things so we can make this
  #       part of the software more readable
  SCOPE = view_handler.get_view("WelcomeScreen").__class__

  def run(self, *matches):
    """ Stuff
    """

    # NOTICE
    # This is a working but not normal way for a function to retain its own values
    # between calls. Commands should probably not have internal states and 
    # this way of doing it is kind of hacky.
    if(hasattr(self, 'counter')):
      self.counter += 1 # pylint: disable=E0203
    else:
      self.counter = 0
    view_handler.set_info("Incrementing counter: " + str(self.counter))

    if(self.counter == 2):
      view_handler.add_view(
        view_handler.get_view("TestWelcomeScreen")([
          "Changed view to show that h does not work anymore cause the h function is only registered to the WelcomeScreen view"
        ])
      )
