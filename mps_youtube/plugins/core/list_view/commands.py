class ListViewChangePageCommand(baseTypes.Command):
  REGEX = r'^(n|p)$'

  def triggers(self, scope):
    return issubclass(scope, view_handler.get_view("ListView"))

  def run(self, command):
    cur_view = view_handler.current_view()
    if command == 'n':
      cur_view.set_page(min(cur_view.num_pages(), cur_view.page +1))
    if command == 'p':
      cur_view.set_page(max(0, cur_view.page - 1))

    view_handler.set_info("Showing page {}".format(
      view_handler.colored_text('g', str(cur_view.page + 1))))

class VideoListViewSelector(baseTypes.Command):
  REGEX = r'^([\d, -]+){1,250}\s*([a-z ]+)*$'

  def triggers(self, scope):
    return issubclass(scope, view_handler.get_view('VideoListView'))

  def run(self, numbers, flags):
    numbers = [x.strip() for x in numbers.split(",")]
    expanded_numbers = []
    for number in numbers:
      print("N", number)
      if "-" in number:
        start, end = number.split("-")
        print("starting on ", start, "ending on ", end)
        expanded_numbers += list(range(int(start), int(end)+1))
      else:
        expanded_numbers += [int(number)]

    expanded_numbers = list(map(lambda x: x - 1, expanded_numbers))

    view = view_handler.current_view()
    expanded_numbers = [view.objects[x] for x in expanded_numbers]
    
    print("At this point these objects will be sent to the player handler", expanded_numbers)
    print("These objects would contain the following youtube video ids that would be searched and played", list(map(lambda x: x.__player__(), expanded_numbers)))