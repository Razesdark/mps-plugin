
class ShowDebugInformationEvent(baseTypes.Event):
  @staticmethod
  def triggers(context):
    return context == "debug.show.info"
  
  @staticmethod
  def run(contextObject):
    print("DEBUG INFORMATION")

class ShowDebugInformationCommand(baseTypes.GlobalCommand):
  REGEX = r'^debug'

  def run(self, matches, *args):
    event_handler.trigger_event("show.info", None)



