""" PLuginHandler loads and manages plugins and their contexts
"""
import os
import copy

from typing import List, Dict
from inspect import isfunction

import mps_youtube.base_types
from mps_youtube.base_types import View, Command, Event
from mps_youtube.query_handler import Query
from mps_youtube.terminal import TerminalInformation
from mps_youtube.plugin_handler import FakeModule, PluginHandlerContextWrapper, Plugin
from mps_youtube.view_handler import ViewHandlerContextWrapper
from mps_youtube.event_handler import EventHandlerContextWrapper
from mps_youtube.util import uea_pad


class PluginHandler:
  """
    PluginHandler controls loading, registering
  """
  plugin_directory: str = "mps_youtube/plugins"
  def __init__(self, c) -> None:
    self.core = c
    self.plugins: Dict[str, classmethod] = dict()
    self._define_base_types()
    self._create_standard_globals_template()
    self.loaded_modules: Dict = dict()
    self.plugin_pool = None
    folder_list: List[str] = os.listdir(self.plugin_directory)
    folder_list = [Plugin(x) for x in folder_list]
    folder_list = filter(lambda x: x.is_plugin(), folder_list)

    # this pool is used to check if any of the plugins has dependency issues
    self.plugin_pool = {x.name: x for x in folder_list}
    folder_list = list(self.plugin_pool.values())
    folder_list = map(lambda x: x.verify_dependencies_against_pool(self.plugin_pool), folder_list)
    folder_list = list(filter(lambda x: len(x) > 0, folder_list))

    if folder_list:
      for error in folder_list:
        for error_code in error:
          print("  * {}\r\n".format(error_code))
      exit(-10)

    # Force core being loaded first
    self._load_plugin('core', self.plugin_pool['core'])
    for name, plugin in self.plugin_pool.items():
      self._load_plugin(name, plugin)

  def loaded_plugins(self) -> int:
    """Return number of plugins loaded
    """
    return len(self.loaded_modules.keys())

  def _is_loaded(self, name: str) -> bool:
    return name in self.loaded_modules.keys()

  def _load_plugin(self, name: str, plugin: Plugin) -> None:
    for _name, _plugin in plugin.get_dependencies_from_pool(self.plugin_pool):
      if not self._is_loaded(_name):
        self._load_plugin(_name, _plugin)
    self.loaded_modules[name] = dict()

    _scope: Dict = plugin.global_scope(self.get_standard_globals())
    _scope['view_handler'] = ViewHandlerContextWrapper(plugin.name, self.core.view_handler)
    _scope['plugin_handler'] = PluginHandlerContextWrapper(plugin.name, self)
    _scope['event_handler'] = EventHandlerContextWrapper(plugin.name, self.core.event_handler)
    _scope['Query'] = Query

    _locals: Dict = dict()
    _imported: List = list()

    if name in self.std_global.keys():
      return
    else:
      self.std_global[name] = FakeModule(dict())
    for code in plugin.get_code():
      exec(code, _scope, _locals) # pylint: disable=W0122
      self.std_global[name].append(_locals)

      for key, value in _locals.items():
        if key in _imported:
          continue
        if isfunction(value):
          pass
        elif key == '__doc__':
          pass
        elif issubclass(value, View):
          self.core.view_handler.register_view(value)
          _imported.append(key)
        elif issubclass(value, Command):
          self.core.command_handler.register_command(value)
          _imported.append(key)
        elif issubclass(value, Event):
          self.core.event_handler.add_event(value)
          _imported.append(key)

    self.std_global['plugins'][name] = FakeModule(_locals.copy())

  def _define_base_types(self):
    ## Create the baseTypes dict
    self.base_types = dict()
    self.base_types['View'] = mps_youtube.base_types.View
    self.base_types['SingleDrawView'] = mps_youtube.base_types.SingleDrawView
    self.base_types['Command'] = mps_youtube.base_types.Command
    self.base_types['GlobalCommand'] = mps_youtube.base_types.GlobalCommand
    self.base_types['Event'] = mps_youtube.base_types.Event
    self.base_types['TransformationEvent'] = mps_youtube.base_types.TransformationEvent
    self.base_types['TerminalInformation'] = TerminalInformation
    self.base_types['QueryItem'] = mps_youtube.base_types.QueryItem

  def _create_standard_globals_template(self) -> None:
    self.std_global: Dict = dict()
    # Find a better place to put this
    self.std_global['uea_pad'] = uea_pad
    self.std_global['__builtins__'] = dict()
    self.std_global['plugins'] = dict()
    self.std_global['baseTypes'] = FakeModule(self.base_types)

    def add_builtin(name):
      """Add a builtin to the standard global context"""
      self.std_global['__builtins__'][name] = __builtins__[name]

    # Standard builtins
    add_builtin("__build_class__")
    add_builtin("str")
    add_builtin("__name__")
    add_builtin("int")
    add_builtin("bool")
    add_builtin('exit')

  def get_standard_globals(self) -> Dict:
    """returns a deep copy of the standard globals
    """
    return copy.deepcopy(self.std_global)
