
import json
import os
import re

from typing import Tuple, Dict, List

PLUGIN_DIRECTORY = "mps_youtube/plugins"

class PluginLoadException(Exception):
  pass

class NotPluginException(Exception):
  pass

def str_to_version_tuple(version_string: str) -> Tuple:
  # Remove all non numerical and . characters
  text = re.sub(r'([^1-9\.]+)', '', version_string)
  # Exctract numbers
  parts = tuple(map(int, text.split(".")))

  if len(parts) == 3:
    return parts + tuple([0])
  return parts[0:4]

class Plugin:
  def __init__(self, name):
    self.name = name

    if self.is_plugin():
      self.manifest = json.load(open("{0}/{1}/manifest.json".format(PLUGIN_DIRECTORY, self.name)))
      self.name = self.manifest["name"]

  def is_plugin(self):
    # TODO: Add version regex check
    return os.path.isfile("{}/{}/manifest.json".format(PLUGIN_DIRECTORY, self.name))

  def get_code(self):
    def extract_code(filename):
      filename = filename.replace("..", "")
      lines = open("{}/{}/{}".format(PLUGIN_DIRECTORY, self.name, filename)).readlines()
      return "\r\n".join(lines)
    return list(map(extract_code, self.manifest['source-files']))

  def get_version(self):
    return str_to_version_tuple(self.manifest['version'])

  def is_higher_version_than(self, other: Tuple) -> bool:
    this = self.get_version()
    for _this, _other in zip(this, other):
      if _other > _this:
        return False
    return True

  def get_dependencies_from_pool(self, pool: Dict) -> List[Dict]:
    dep_names = [x['name'] for x in self.manifest['dependencies']]
    return  list(filter(lambda x: x[0] in dep_names, pool.items()))

  def verify_dependencies_against_pool(self, pool: Dict) -> List: # pylint: disable=C0103
    # No dependencies key, means no dependencies
    if not "dependencies" in self.manifest.keys():
      return []

    def check_dep(dependency):
      if not pool[dependency['name']].is_higher_version_than(str_to_version_tuple(dependency['version'])):
        return "{} requires version {} of {}, but {} was found".format(
          self.name,
          dependency['version'],
          dependency['name'],
          pool[dependency['name']].manifest["version"])
      return None

    deps = map(check_dep, self.manifest['dependencies'])
    deps = filter(lambda x: not x is None, deps)
    return list(deps)

  def global_scope(self, plugin_scope: Dict) -> Dict:
    manifest = self.manifest

    # Add default builtins
    def add_builtin(name):
      plugin_scope['__builtins__'][name] = __builtins__[name]

    for builtin in self.manifest['builtins']:
      if builtin in __builtins__:
        add_builtin(builtin)
      else:
        raise PluginLoadException(
          "The plugin {} tried to load invalid global named {}".format(self.name, builtin)
        )

    # Import a list of modules listed in the manifest file
    for module in manifest['modules']:
      if not module in plugin_scope.keys():
        plugin_scope[module] = __import__(module)
    return plugin_scope

  def to_s(self):
    string = "::{}\r\n{}".format(self.name, "#" * (len(self.name) + 3))
    string += "\r\n\t{}".format("Valid" if self.is_plugin() else "This is not a valid plugin")
    if self.is_plugin():
      string += "\t Version: {}".format(self.get_version())
    return string
