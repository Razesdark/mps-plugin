# pylint: disable=too-few-public-methods
class PluginHandlerContextWrapper:
  """ A plugin handler wrapper that prevents accidental access to "private"
      plugin handler functions.
  """
  def __init__(self, name, pluginhandler):
    self.__name = name
    self.__plugin_handler = pluginhandler

  def loaded_plugins(self) -> int:
    """ Get number of loaded plugins
    """
    return self.__plugin_handler.loaded_plugins()
