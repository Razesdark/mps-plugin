"""
  DeepCopy wont work on modules, so creating a dictionary based abstraction
  abstraction so we can create module-like chuncks of code that we can pass to
  the exec context.
"""
from typing import Dict, Callable

class FakeModule:
  """A module-like abstraction of a dictionary
  """
  def __init__(self, objs: Dict) -> None:
    self.items = objs

  def __getattr__(self, key):
    if key in self.items.keys():
      return self.items[key]
    raise KeyError("Object " + key + " does not exist")

  def append(self, _locals: Dict[str, Callable]) -> None:
    """ appends key/values from a dictionary to the FakeModule
    """
    for key, value in _locals.items():
      if not key in self.items:
        self.items[key] = value

  def has_function(self, name):
    """ See if he module has a function by this name
    """
    return name in self._locals

  def __str__(self):
    return str(self.items)

  def __deepcopy__(self, _):
    return FakeModule(self.items.copy())
