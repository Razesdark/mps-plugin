# mps-youtube
## Plugin edition

All global variables previously held by mpsyt is now being handled by the core class.
The core object will handle such things as state, plugins, text input, settings.

One of the things I'm trying to tackle with this version of the software is state management, nested views and extensibility.

### Nested Views
The software will have an array of views. The plugin authors have the option to add and remove views from the stack in a *first-in-last-out* fashion. This enables other plugins to exend base (or other plugin functionality) without having issues similar to current mps-youtube where only one view can exist in memory.

### Function extensibility
This project intend to continue with the same kind of command structure that the existing version of mps-youtube uses. However instead of only global commands, we also want to have scoped functions.

The point is to be able to register a command to a particular view type. This way any plugin can extend core or other plugin functionality.
