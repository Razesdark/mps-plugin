manifest.json
===================

The manifest file lets you describe your plugin. It manages version and loading plugins in the correct order.

Below is a sample of a `manifest.json` file.

```json
{
  "name": "core",
  "version": "0.0.0.1",
  "description": {
    "short": "The core functionality of mps-youtube",
    "long": "The core functionality of mps-youtube, including\r\nsearching and playing videos"
  },
  "modules": [
    "os",
    "typing"
  ],
  "requirements": [
    {"name": "other-plugin", "version": "0.2.4.5" }
  ],
  "builtins": [
    "max"
  ],
  "source-files": [
    "a_file.py",
    "some_folder/b_file.py"
  ],
  "installation": {
    "settings": [
      {"name": "test_variable", "type": "bool", "default": "True"}
    ]
  }
}
```

## Configrations
One of the key goals with the manifest is to enable a minimum level of insight into which functionality a 
plugin uses, so the user can make a good decision on wether or not they are going to actually load the plugin.

The plugin handler is aiming to give access to safe core functionality to the application so that importing and using
potentially risky modules and functions are kept to a minimum.
### name
The name of the application, it is customary to use lowercase first letter and use camel case. The plugin that are being executed will have access to
all other loaded plugins through a `plugins[name]` context or simply `name`, unless it is taken or name is invalid. (Such as spaces, hyphen, etc).

### version
The version field is used to make sure 
The software can reason over version numbers following the [Semantic Versioning](http://semver.org) standard.
We only reason over the `major`, `minor` and `patch` field. If the `patch` field contains any text, it will 
be stripped prior to doing any reasoning. The software will not be able to understand that `1.0.0-alpha` is prior or
after `1.0.0-beta` as the `-alpha` and `-beta` will be stripped.

### description
The `short` description is used when listing plugins in a list view. There is technically no max or minimum length for 
this field, all usages of the `short` should be limited to a single line with no line breaks.

The `long` description is used when loading the plugin. Again, the maximum size here is dependent on terminal resolution.

### modules 
A list of standard or installed modules that will be loaded and available to the plugin. Importing files is disabled for plugins
so all modules needed will have to be declared here.

### requirements
Is used to list other plugins that needs to be loaded prior to this plugin being loaded, it is supplied as a hash with a `name` and a `version` where the
version is the minimum version needed. There is currently no support for loading different version of same plugin in paralell or requiring a maximum version.

Please consider this when developing your plugin.

### builtins
Most builtins are also locked behind this setting. Except for rudementary functionality such as `__build_class__`, `str`, `int` etc. all builtins are locked away unless the plugin asks for permissions to use them, by listing them in this file.

### source-files 
By default only `__init__.py` is loaded by the application. If other files are needed, they will be prepended to the executed stream of code and will have the same global and local scope as `__init__.py`. *Filenames should be listed as dynamic paths and should only list files in the plugin folder or sub-folders of the current plugin's folder*.

### installation
Holds all default variables and other variables that needs to be exposed and set before the application loads the plugin. The app tracks if the
application version is the same as it was on last startup. If it has changed to a higher number, the installation code and default values will be copied
to the settings handler. If it is an upgrade process, it will not replace values that exist but will append potentially new settings to the app settings file.

Currently it only supports the `settings` sub-key. 

#### Datatypes
Currently supported datatypes are the standard json datatypes. 

* `str`
* `int`
* `float`
* `bool`.
