Command Namespaces
====================

some commands are being registered as "namespace commands". These special namespaces simple commands that takes no arguments, but call a transformation event when run, which mutates the text output from the function.

For example is the `help` command. The `help` command will list the core functionality. In order for 
plugins to append information to `help`, it will trigger a transformation event that lets plugins append information about their plugins.