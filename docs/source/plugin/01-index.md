Plugins
======================

We as the core development team do not want to maintain a curated list of available plugins
or in a sense create our own package management solution, therefore we are instead trying 
to give users a better toolkit for deciding wether or not they want to activate plugins.

To enable this philosophy we are not merely loading the plugin python code as normal code, but create
separate virtual enviroments using the built in `exec` functionality, but with custom `globals` and `locals` for each plugin, giving them separate runtimes that include all previously loaded plugins. Some wrappers that expose core parts of mpsyt and its handlers.

## Creating a plugin
In order to successfully create a plugin for mps-youtube, you will need to understand the basics of our component based architecture and the layout of our `manifest.json` file.

### Component based architecture
A mps-youtube plugin consists of a list of components that after being defined is added to its respective handlers. For example, if you define a class inheriting from `baseTypes.View` in the root
of any of your files. It will after the file has been fully executed be recognized as a `View` component by the *plugin handler*, which will in turn add it as a retrievable view in the `ViewHandler`.

All different components has a base type that it must inherit from. All of these types are available
from the `baseTypes` module in the plugin context.

### Component base types
#### Views
##### baseTypes.View
A `View` is any kind of component that is used to display information on the screen. It requires
a function called `draw` which takes an argument of type `TerminalInformation` and must return a `str` or `List[str]`.

```python3
  class ListView(baseTypes.View):
    def __init__(self, listItems):
      self.listItems = listItems

    def draw(self, term_info):
      str = ""
      str += some_code_()
      return str
```

Unless for a good reason, it is expected that a view fills the entire screen, based on the sizes described in the supplied `TerminalInformation` object. This object subtracts space for the status bar as well as the command line.

##### baseTypes.SingleDrawView
There is a subclass of `baseTypes.View` called `baseTypes.SingleDrawView` which is used by modules like the `debug` module. This is in effect a view that is put on the view stack only to be drawn once then discarded. This is mostly used when you need to just dump some information on the screen
once. It acts 

#### Commands
##### baseTypes.Command
The command class is a command definition that is only accessable in one or more views. It requires
a regex and a scope, both declared as `REGEX` and `SCOPE` as variables on the class. and a `run` function that takes regex matches as an argument.

Scope is either a `str` or a `List[str]`, containing the names of the views this command is supposed to work on. There is no checking if a command regex overlaps with another, so bear that in mind when
coming up with different regexes. 

In general short form command names should be reserved for either very core functionality (like `n` and `p` for changing pages in your view) or ease of use versions of longer names(for example `download_url` to `dlurl`). 

It is considered a plus if you can put all your commands behind the same starting keyword. If your plugin is downloading files in different shapes and forms having several commands like `download audio` and `download video` is considered a plus.

```python3
  class ListViewChangePageCommand(baseTypes.Command):
    REGEX = r'(n|p)'
    SCOPE = 'ListView' 

    def run
```