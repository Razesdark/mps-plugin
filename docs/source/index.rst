mps-youtube
===================
:Version: 0.9.0.0

Welcome to the documentation for `mps-youtube`. This, second iteration of the
mps-youtube project is taking the simplicity of the original mps-youtube application
and remakes it as a capable open and hackable system.

This documentation is hopefully enough to get you comfortable with the systems and 
design philosohies that were utilized in this piece of software.

  .. toctree::
    :maxdepth: 2
    :caption: Plugin documentation

    plugin/01-index
    plugin/03-the-package
    plugin/05-manifest
    plugin/09-namespaces

Events
---------------

.. toctree::
    :maxdepth: 2
    :caption: Events

    events/event_types
    events/core