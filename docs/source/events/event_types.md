# Event Types

There are two distinct types of events that are used in mps-youtube.

## Standard Event
The standard event is a one way event call where the event caller does not get any feedback
of actions taken by the event recipients.

## TransformationEvent
The transformation event is an event type where the supplied information is transformed and returned to the
event caller.