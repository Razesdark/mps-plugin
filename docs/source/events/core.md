# Core Events

Here is a list of all events in the core application

## Post Plugins
  * `name`: `core.post.plugins`
  * `context`: `None`

The post plugins event is triggered before the main loop of the program. 